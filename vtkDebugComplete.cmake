# Activate testing
SET(VTK_BUILD_TESTING "WANT" CACHE STRING "")
SET(VTK_BUILD_EXAMPLES ON CACHE BOOL "")

# Debug build
SET(CMAKE_BUILD_TYPE "Debug" CACHE STRING "")

# Python, always needed for complete testing
SET(VTK_WRAP_PYTHON ON CACHE BOOL "")

# MPI, always needed for complete testing
SET(VTK_USE_MPI ON CACHE BOOL "")

# TBB SMP, for complete testing
SET(VTK_SMP_IMPLEMENTATION_TYPE "TBB" CACHE STRING "")

# Enable a bunch of warnings, to catch them before the dashboards
SET(CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -Woverloaded-virtual -Wno-deprecated -Wno-strict-overflow -Wno-array-bounds -Wunreachable-code -fdiagnostics-color=always" CACHE STRING "")
SET(CMAKE_C_FLAGS "-Wall -Wextra -Wshadow" CACHE STRING "")

# Enable CCache
SET(CMAKE_CXX_COMPILER_LAUNCHER "ccache" CACHE STRING "")

# Store data and baselines
SET(VTK_DATA_STORE "/home/tgalland/VTK/VTKExternalData/" CACHE PATH "")

# Export compile commands
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE BOOL "")

# Stop forgetting this one !
SET(VTK_DEBUG_LEAKS ON CACHE BOOL "")

# To be closer to the buildbots
SET(VTK_LEGACY_REMOVE ON CACHE BOOL "")
