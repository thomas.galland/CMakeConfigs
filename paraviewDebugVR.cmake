# Debug build
SET(CMAKE_BUILD_TYPE "Debug" CACHE STRING "")

# Python, always needed for complete testing
SET(PARAVIEW_USE_PYTHON ON CACHE BOOL "")

# Enable XR (OpenVR + OpenXR)
SET(PARAVIEW_PLUGIN_ENABLE_XRInterface ON CACHE BOOL "")
SET(VTK_MODULE_ENABLE_VTK_RenderingVR "WANT" CACHE STRING "")
SET(VTK_MODULE_ENABLE_VTK_RenderingOpenVR "WANT" CACHE STRING "")
SET(VTK_MODULE_ENABLE_VTK_RenderingOpenXR "WANT" CACHE STRING "")

# OpenVR headers and library paths (change path if needed)
SET(OpenVR_LIBRARY "\tgalland\OpenVR\lib\win64\openvr_api.lib" CACHE STRING "")
SET(OpenVR_INCLUDE_DIR "\tgalland\OpenVR\headers" CACHE STRING "")

# OpenXR headers and library paths (change path if needed)
SET(OpenXR_LIBRARY "\tgalland\OpenXR\Build\src\loader\openxr_loaderd.lib" CACHE STRING "")
SET(OpenXR_INCLUDE_DIR "\tgalland\OpenXR\Build\include\openxr" CACHE STRING "")

# Enable a bunch of warnings, to catch them before the dashboards
SET(CMAKE_CXX_FLAGS "-Wall -Wextra -Wshadow -Woverloaded-virtual -Wno-deprecated -Wno-strict-overflow -Wno-array-bounds -Wunreachable-code -fdiagnostics-color=always" CACHE STRING "")
SET(CMAKE_C_FLAGS "-Wall -Wextra -Wshadow" CACHE STRING "")

# Enable CCache
SET(CMAKE_CXX_COMPILER_LAUNCHER "ccache" CACHE STRING "")

# Export compile commands
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON CACHE BOOL "")

# Stop forgetting this one !
SET(VTK_DEBUG_LEAKS ON CACHE BOOL "")
